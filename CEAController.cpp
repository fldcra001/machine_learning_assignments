/**
    )                                                         
 ( /(                                (         )              
 )\())  (   (  (         (    )      )\  (  ( /((             
((_)\  ))\ ))\ )(   (    )\  /((  ( ((_)))\ )\())\  (   (     
 _((_)/((_)((_|()\  )\  ((_)(_))\ )\ _ /((_|_))((_) )\  )\ )  
| \| (_))(_))( ((_)((_) | __|)((_|(_) (_))(| |_ (_)((_)_(_/(  
| .` / -_) || | '_/ _ \ | _|\ V / _ \ | || |  _|| / _ \ ' \)) 
|_|\_\___|\_,_|_| \___/ |___|\_/\___/_|\_,_|\__||_\___/_||_|  
                                              
	   (                )        (  (           
	   )\            ( /((       )\ )\  (  (    
	 (((_)  (   (    )\())(   ( ((_|(_)))\ )(   
	 )\___  )\  )\ )(_))(()\  )\ _  _ /((_|()\  
	((/ __|((_)_(_/(| |_ ((_)((_) || (_))  ((_) 
	 | (__/ _ \ ' \))  _| '_/ _ \ || / -_)| '_| 
	  \___\___/_||_| \__|_| \___/_||_\___||_|   

	  Modified by Craig Feldman
	  October 2014
                                            
*/
#include "CEAController.h"


CEAController::CEAController(HWND hwndMain):
	CContController(hwndMain)
{	
}

/* Initialise GA object and inserts random ANN into sweepers */
void CEAController::InitializeLearningAlgorithm(void)
{
	const double CROSSOVER_RATE = 0.7;
	const double MUTATION_RATE = 0.02;
	const double MAX_MUTATION = 0.3;

	GA = new CBasicEA(CParams::iNumSweepers, m_vecSweepers[0]->getNumWeights(), CROSSOVER_RATE, MUTATION_RATE, MAX_MUTATION);

	population = GA->getPopulation();

	//insert the starting random weight values into sweepers brains
	for (int i = 0; i < m_vecSweepers.size(); ++i) {
		m_vecSweepers[i]->setWeights(population[i].weights);
	}
}

/* Returns the dot product between the sweeper's look vector and the vector from the sweeper to the object */
inline double dot_between_vlook_and_vObject(const CContMinesweeper &s, const CContCollisionObject &o) {
	SVector2D<double> vLook = s.getLookAt();
	SVector2D<double> pt = o.getPosition();
	//get the vector to the point from the sweepers current position:
	SVector2D<double> vObj(SVector2D<double>(pt.x, pt.y) - s.Position());
	Vec2DNormalize<double>(vObj);

	return Vec2DDot<double>(vLook, vObj);
}


bool CEAController::Update(void) {
	uint cDead = std::count_if(m_vecSweepers.begin(), m_vecSweepers.end(), [](CContMinesweeper * s)->bool{ return s->isDead();} );

	if (cDead == CParams::iNumSweepers) {
		printf("All dead ... skipping to next iteration\n");
		m_iTicks = CParams::iNumTicks;
	}

	// Completed a generation
	if (m_iTicks == CParams::iNumTicks){
		// Set the fitness of the sweepers in the genome vector.
		for (int i = 0; i < m_vecSweepers.size(); ++i)
			population[i].fitness = m_vecSweepers[i]->getFitness();
		
		// Sort by fitness for selection 
		sort(population.begin(), population.end());

		// Run the population through one generation (performs crossover and mutation)
		population = GA->run(population);

		// Update sweepers ANNs with the new next generation weights
		for (int i = 0; i < m_vecSweepers.size(); ++i)
			m_vecSweepers[i]->setWeights(population[i].weights);
	}

	CContController::Update(); //call the parent's class update. Do not delete this.

	for (auto s = m_vecSweepers.begin(); s != m_vecSweepers.end(); ++s) {

		// Compute inputs to ANN		
		double dot_mine = dot_between_vlook_and_vObject(**s, *m_vecObjects[(*s)->getClosestMine()]);
		double dot_supermine = dot_between_vlook_and_vObject(**s, *m_vecObjects[(*s)->getClosestSupermine()]);
		double dist_supermine = Vec2DLength(m_vecObjects[(*s)->getClosestSupermine()]->getPosition() - (*s)->Position());
		double dist_mine = Vec2DLength(m_vecObjects[(*s)->getClosestMine()]->getPosition() - (*s)->Position());
		
		std::vector<double>inputs = {dot_mine, dot_supermine, dist_mine, dist_supermine};

		if ((*s)->getNeuralNet()->classify(inputs) == 0){ 
			// turn towards the mine
			//std::cout << "TURNING TOWARDS" << std::endl;
			SPoint pt(m_vecObjects[(*s)->getClosestMine()]->getPosition().x, m_vecObjects[(*s)->getClosestMine()]->getPosition().y);
			(*s)->turn(pt, 1);
		}
		else {
			//turn away from a supermine
			//std::cout << "TURNING AWAY" << std::endl;
			SPoint pt(m_vecObjects[(*s)->getClosestSupermine()]->getPosition().x, m_vecObjects[(*s)->getClosestSupermine()]->getPosition().y);
			(*s)->turn(pt, 1, false);
		}
	}
	return true; //method returns true if successful. Do not delete this.
}

/* Destructor - deallocates GA pointer */
CEAController::~CEAController(void) {
	if (GA)
		delete GA;
}

