#pragma once
#include "cdisccontroller.h"
#include "CParams.h"
#include "CDiscCollisionObject.h"
#include <cmath>
#include "SVector2D.h"
#include "State.h"

typedef unsigned int uint;
class CQLearningController :
	public CDiscController
{
private:
	uint xDim;
	uint yDim;
	uint numStates;
	uint numActions;

	std::vector<std::vector<double> > Q;

public:
	CQLearningController::CQLearningController(HWND hwndMain) :
		CDiscController(hwndMain),
		xDim(CParams::WindowWidth / CParams::iGridCellDim + 0),
		yDim(CParams::WindowHeight / CParams::iGridCellDim + 0)
		{};
	
	virtual void InitializeLearningAlgorithm(void);

	double R(CDiscMinesweeper * sw);

	virtual bool Update(void);


	virtual ~CQLearningController(void);

};

