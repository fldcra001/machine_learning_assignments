/*
                                                                           
   (               )                                        )              
 ( )\     )     ( /(       (                  (  (     ) ( /((             
 )((_) ( /(  (  )\())`  )  )(   (  `  )   (   )\))( ( /( )\())\  (   (     
((_)_  )(_)) )\((_)\ /(/( (()\  )\ /(/(   )\ ((_))\ )(_)|_))((_) )\  )\ )  
 | _ )((_)_ ((_) |(_|(_)_\ ((_)((_|(_)_\ ((_) (()(_|(_)_| |_ (_)((_)_(_/(  
 | _ \/ _` / _|| / /| '_ \) '_/ _ \ '_ \) _ \/ _` |/ _` |  _|| / _ \ ' \)) 
 |___/\__,_\__||_\_\| .__/|_| \___/ .__/\___/\__, |\__,_|\__||_\___/_||_|  
                    |_|           |_|        |___/                         

                                            
			   (                )        (  (           
			   )\            ( /((       )\ )\  (  (    
			 (((_)  (   (    )\())(   ( ((_|(_)))\ )(   
			 )\___  )\  )\ )(_))(()\  )\ _  _ /((_|()\  
			((/ __|((_)_(_/(| |_ ((_)((_) || (_))  ((_) 
			 | (__/ _ \ ' \))  _| '_/ _ \ || / -_)| '_| 
			  \___\___/_||_| \__|_| \___/_||_\___||_|   

	Controller class for backpropagation
	
	modified by Craig Feldman
	24 Aug 2014
                                            
 */

#include "CBackPropController.h"


CBackPropController::CBackPropController(HWND hwndMain):
	CContController(hwndMain)
{
	f = ifstream(CParams::sTrainingFilename.c_str());
}

void CBackPropController::InitializeLearningAlgorithm(void)
{
	CContController::InitializeLearningAlgorithm(); //call the parent's learning algorithm initialization
	
	//read training data from file (this is pretty basic text file reading, but at least the files can be inspected and modified if necessary)
	
	uint no_training_samples;
	uint dist_effect_cutoff;
	uint no_inputs;
	uint no_hidden;
	uint no_out;
	double learning_rate;
	double mse_cutoff;
	//ifstream f(CParams::sTrainingFilename.c_str());
	assert(f.is_open());

		f >> no_training_samples;
		f >> no_inputs;
		f >> no_hidden;
		f >> no_out;
		f >> learning_rate;
		f >> mse_cutoff;

		CBackPropController::readTrainingData(no_training_samples, no_inputs, no_out);
	//init the neural net and train it
		_neuralnet = new CNeuralNet(no_inputs,no_hidden,no_out,learning_rate,mse_cutoff);
		//_neuralnet->train((const double **)inp,(const double **)out,no_training_samples);

		// while the MSE is greater than the threshold, continue to train.
		while ((_neuralnet->train(inputs, outputs, no_training_samples) > mse_cutoff) && (!f.eof())) {
			readTrainingData(no_training_samples, no_inputs, no_out);
		}

		f.close();

		std::cout << "\nNetwork successfully trained." << std::endl;

}

/* Reads in no_training_samples of training data at a time */
void CBackPropController::readTrainingData(uint no_training_samples, uint no_inputs, uint no_out) {
	double n;
	inputs.clear();
	outputs.clear();

	//Read in the training data
	for (uint32_t i = 0; i < no_training_samples && !f.eof(); ++i){
		//printf("Reading file ... %f%%\n",i / float(no_training_samples)*100.0);
		inputs.push_back(std::vector<double>());
		outputs.push_back(std::vector<double>());

		//read input data
		for (uint32_t inp_s = 0; inp_s < no_inputs && !f.eof(); ++inp_s){
			f >> n;
			inputs[i].push_back(n);
		}
		//read output data
		for (uint32_t out_s = 0; out_s < no_out && !f.eof(); ++out_s){
			f >> n;
			outputs[i].push_back(n);
		}
	}
}

/**
Returns the dot product between the sweeper's look vector and the vector from the sweeper to the object
*/
inline double dot_between_vlook_and_vObject(const CContMinesweeper &s,const CContCollisionObject &o){
	SVector2D<double> vLook = s.getLookAt();
	SVector2D<double> pt = o.getPosition();
		//get the vector to the point from the sweepers current position:
		SVector2D<double> vObj(SVector2D<double>(pt.x,pt.y) - s.Position());
		Vec2DNormalize<double>(vObj);
		//remember (MAM1000 / CSC3020) the dot product between two normalized vectors returns
		//1 if the two vectors point in the same direction
		//0 if the two vectors are perpendicular
		//-1 if the two vectors are pointing in opposite directions
		return Vec2DDot<double>(vLook,vObj);
}

bool CBackPropController::Update(void)
{
	CContController::Update(); //call the parent's class update. Do not delete this.
	for (auto s = m_vecSweepers.begin(); s != m_vecSweepers.end(); ++s){

	}

	return true; //method returns true if successful. Do not delete this.
}

CBackPropController::~CBackPropController(void)
{
	delete _neuralnet;
}
