#CSC022H ML Assignment#

Craig Feldman (2014)

### Info ###

This project involves using three different machine learning algorithms to steer a set of autonomous minesweepers in a two-dimensional world of mines, rocks and supermines. The minesweepers must learn to collect mines and avoid the other obstacles. The three ML algorithms are:

1. Backpropagation
2. Q-Learning
3. Neuro-Evolution

**Additional information can be found in the assignment instructions in the 'Downloads' section.**
### Instructions ###

1. Import the solution ("smart sweepers.sln") into Visual Studio.
2. Run the application in Visual Studio.

The driver file 'main.cpp' allows you to easily switch between the 3 algorithm controller classes. You only need to modify the line after the includes section to one of the following:


```
#!c++

/*
Choices:
typedef CEAController PRAC_ALGORITHM; //Evolutionary Algorithm
typedef CBackPropController PRAC_ALGORITHM; //Backpropagation Algorithm
typedef CQLearningController PRAC_ALGORITHM; //Q-Learning Algorithm
*/

//This will start the solution with the CEAController class:
typedef CEAController PRAC_ALGORITHM;
```

You can modify some algorithm parameters in the parameters.ini file.

---

Information about the results achieved for each algorithm can be found in the 'Downloads' section.

**Enjoy!**