#ifndef STATE_H
#define STATE_H
/**
Stores info about the state (used to store previous state)
Author: Craig Feldman
26 Sep 2014
*/
struct State
{
	int stateIndex;
	int actionIndex;
	bool alive = true;
};

#endif