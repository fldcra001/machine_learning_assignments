/*
Used to perform evolutionary procedures on the agents.

Craig Feldman
October 2014
*/

#pragma once
#include "cneuralnet.h"
#include "CContMinesweeper.h"
#include <vector>
#include <assert.h>
#include <algorithm>

/* Stores an agents weights and fitness */
struct Genome {
	vector<double> weights;
	double fitness;

	Genome() : fitness(0){};
	Genome(std::vector<double> w, double f) : weights(w), fitness(f) {};

	//overload '<' used for sorting
	friend bool operator<(const Genome& lhs, const Genome& rhs)	{
		return (lhs.fitness < rhs.fitness);
	}	
};

class CBasicEA {
private:

	int populationSize;
	int numWeights;
	int generationNum;

	double totalFitness, bestFitness, averageFitness, worstFitness, totalModifiedFitness;
	int fittestGenome;
	int numElite; // increase elitism as we proceed

	// Probability of crossover and mutation; maximum amount to mutate by; how much to reduce mutation by with time
	static double crossOverRate, mutationRate, maxMutation, mutationReduction;

	// Holds entire population of genomes
	vector<Genome> population;

	/* Performs Crossover on two parents */
	static void crossover(const vector<double>& parent1, const vector<double>& parent2, vector<double>& child1, vector<double>& child2);

	/* Performs mutation on weight vector */
	static void mutate(vector<double>& weights);

	/* Performs roulette wheel sampling to return a genom */
	Genome getGenomeRoulette();

	/* Sets avg, best, worst etc fitness */
	void setFitnessScores();


public:
	/* Constructs a population of randomly initialised genomes */
	CBasicEA::CBasicEA(int popSize, int numWeights, double crossOver, double mutation, double maxMut);

	/* Performs one evolutionary cycle on population */
	vector<Genome> run(vector<Genome>& oldPopulation);

	/* Returns a vector containing genome for each agent in population */
	vector<Genome> getPopulation() const {
		return population;
	}

	/* Adds elite agents to newPop */
	void addBest(int quantity, int copies, vector<Genome> & newPop);

};

