#pragma once
#include "ccontcontroller.h"
#include "CNeuralNet.h"
#include <assert.h>
class CBackPropController :
	public CContController
{
protected:
	CNeuralNet* _neuralnet;
	ifstream f;

	//training data
	std::vector<std::vector<double> > inputs;
	std::vector<std::vector<double> > outputs;

public:
	CBackPropController(HWND hwndMain);
	virtual void InitializeLearningAlgorithm(void);
	virtual bool Update(void);

	/* reads in more training data from the training file */
	void readTrainingData(uint no_training_samples, uint no_inputs, uint no_out);

	~CBackPropController(void);
};

