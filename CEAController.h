#pragma once
#include "ccontcontroller.h"
#include <vector>
#include "CNeuralNet.h"
#include "CParams.h"
#include "CBasicEA.h"


class CEAController :
	public CContController
{
protected:
	CBasicEA * GA;
	vector<Genome> population;
public:
	CEAController(HWND hwndMain);

	/* Initialise GA object and inserts random ANN into sweepers */
	virtual void InitializeLearningAlgorithm(void);

	virtual bool Update(void);
	virtual ~CEAController(void);
};

