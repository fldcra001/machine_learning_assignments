/*
 * CNeuralNet.h
 *
 *  Created on: 26 Dec 2013
 *      Author: benjamin

 *	Modified by: Craig Feldman
 *	22 August 2014
 */

#ifndef CNEURALNET_H_
#define CNEURALNET_H_
#include <vector>
#include <cmath>
#include <algorithm>
#include <stdlib.h>
#include <cstring>
#include <stdio.h>
#include <stdint.h>
#include "utils.h"

typedef unsigned int uint;
class CBasicEA; //forward declare EA class, which will have the power to access weight vectors

class CNeuralNet {
	friend class CBasicEA;
private:
	uint inputLayerSize;
	uint hiddenLayerSize;
	uint outputLayerSize;

	double lRate;
	double mse_cutoff;

	//weights
	std::vector<std::vector<double>> inputToHiddenWeights;
	std::vector<std::vector<double>> hiddenToOutputWeights;

	//nodes
	std::vector<double> input;
	std::vector<double> hidden;
	std::vector<double> output;

protected:
	/* Feeds the input vector through the ANN and determines the output node values*/
	void feedForward(const std::vector<double> inputs); 

	/* Propagates the error backwards and adjusts weights accordingly*/
	void propagateErrorBackward(const std::vector<double> desiredOutput); 

	/* Computes the MSE of the ANN */
	long double meanSquaredError(const std::vector<double> desiredOutput); 

	/* returns the sigmoid function */
	double sigmoid(double in);

	/* returns the derivative of the sigmoid */
	double sigmoidDerivative(double in);

public:
	/* Constructs the ANN based on the suplied parameters*/
	CNeuralNet(uint inputSize, uint hiddenSize, uint outputLayerSize, double lRate, double mse_cutoff);

	/* Default ANN used by GA */
	CNeuralNet();

	virtual ~CNeuralNet();

	/* Initialse the weights to random values*/
	void initWeights();

	/* Trains the ANN based on the training data*/
	double train(const std::vector<std::vector<double> > inputs, const std::vector<std::vector<double> > outputs, uint trainingSetSize); 

	/* Classifies the given input i.e. gets the output */
	uint classify(const std::vector<double> input); 

	/* Reutrns output at given index */
	double getOutput(uint index) const;

	/* Returns a vector containing all the weights */
	std::vector<double> getWeights() const;

	/* Replaces all weights with given values */
	void setWeights(std::vector<double> &weights);

	/* Returns number of weights in ANN */
	int getNumWeights() const;
};

#endif /* CNEURALNET_H_ */
