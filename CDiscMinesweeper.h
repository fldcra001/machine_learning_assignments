#ifndef CDiscMINESWEEPER_H
#define CDiscMINESWEEPER_H

//------------------------------------------------------------------------
//
//	Name: CMineSweeper.h
//
//  Author: Mat Buckland 2002
//
//  Desc: Class to create a minesweeper object 
//
//------------------------------------------------------------------------
#include <vector>
#include <math.h>
#include "utils.h"
#include "C2DMatrix.h"
#include "SVector2D.h"
#include "CParams.h"
#include "CDiscCollisionObject.h"
#include "CMinesweeper.h"
#include "State.h"

using namespace std;

enum ROTATION_DIRECTION {NORTH=1, SOUTH=3, EAST=0, WEST=2};
class CDiscMinesweeper:public CMinesweeper
{

private:
	//its position in the world
	SVector2D<int>		m_vPosition;
	SVector2D<int>		m_vPrevPosition;
	//direction sweeper is facing
	SVector2D<int>		m_vLookAt;

	//its rotation (surprise surprise)
	ROTATION_DIRECTION	m_dRotation;
	
	//sets the internal closest object variables for the 3 types of objects
	void GetClosestObjects(vector<CDiscCollisionObject*> &objects);

	int m_iSpeed;

	std::vector<std::vector<double> > QTable;
	State prevState;


public:

	CDiscMinesweeper();

	void setRotation(ROTATION_DIRECTION rotForce);
	ROTATION_DIRECTION getRotation();
	
	//updates the information from the sweepers enviroment
	bool			Update(vector<CDiscCollisionObject*> &objects);

	//used to transform the sweepers vertices prior to rendering
	void			WorldTransform(vector<SPoint> &sweeper);

	//checks to see if the minesweeper has 'collected' a mine
	int       CheckForObject(vector<CDiscCollisionObject*> &objects, int size);

	void			Reset();

	void initializeQTable(int numStates, int numActions);



	//-------------------accessor functions
	SVector2D<int>	Position()const{return SVector2D<int>(m_vPosition.x,m_vPosition.y);}
	SVector2D<int>	PrevPosition()const{return SVector2D<int>(m_vPrevPosition.x,m_vPrevPosition.y);}
	/* Returns x co-ord on grid */
	int getXCell() {
		int toReturn = m_vPosition.x / CParams::iGridCellDim;
		if (toReturn == 40)
			return 0;
		return toReturn;
	}

	/* Returns y co-ord on grid */
	int getYCell() {
		int toReturn = m_vPosition.y / CParams::iGridCellDim;
		if (toReturn == 40)
			return 0;
		return toReturn;
	}

	/* Returns info about the prev state */
	State getPrevState() {
		return prevState;
	}

	void setPrevState(State s) {
		prevState = s;
	}

	void setPrevAlive(bool b) {
		prevState.alive = b;
	}


	double getQ(int stateIndex, int actionIndex) {
		return QTable[stateIndex][actionIndex];
	}

	void setQ(int stateIndex, int actionIndex, double value){
		//cout << "setting " << stateIndex << ":" << actionIndex<< " TO " << value << endl;
		QTable[stateIndex][actionIndex] = value;
	}

  
};


#endif

	
	