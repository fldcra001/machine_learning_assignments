/**
         (                                      
   (     )\ )                                   
 ( )\   (()/(   (    ) (        (        (  (   
 )((_)   /(_)) ))\( /( )(   (   )\  (    )\))(  
((_)_   (_))  /((_)(_)|()\  )\ |(_) )\ )((_))\  
 / _ \  | |  (_))((_)_ ((_)_(_/((_)_(_/( (()(_) 
| (_) | | |__/ -_) _` | '_| ' \)) | ' \)) _` |  
 \__\_\ |____\___\__,_|_| |_||_||_|_||_|\__, |  
                                        |___/   

Q learning controller for smart sweepers.

Modified by Craig Feldman
14 Septemeber 2014
*/

#include "CQLearningController.h"

/* Allocates a universal Q table that is shared by all the sweepers */
void CQLearningController::InitializeLearningAlgorithm(void)
{
	using namespace std;
	cout << "Q-Learning\n" << endl;

	cout << "Initialising learning algorithm." << endl;

	numActions = 4;
	numStates = xDim * yDim;

	for (int s = 0; s < numStates; ++s)
		Q.push_back(vector<double>());

	for (int s = 0; s < numStates; ++s)
		for (int a = 0; a < numActions; ++a)
			Q[s].push_back(0);

}

/*
 Returns the reward for a sweepers last action.
 Penalises for movement to encourage exploration.
 Rewards for finding a mine.
 Penalises for finding a supermine.
*/
double CQLearningController::R(CDiscMinesweeper * sw){

	if (sw->getPrevState().alive == false && sw->isDead())
		return 0;
	
	// hit a supermine
	if (sw->getPrevState().alive == true && sw->isDead())
		return -10;


	//see if it's found a mine
	int GrabHit = (sw->CheckForObject(m_vecObjects,	CParams::dMineScale));
	if (GrabHit >= 0) {
		if (!(m_vecObjects[GrabHit])->isDead())
			return 15;
	}

	if (sw->isDead())
		sw->setPrevAlive(false);
	else
		sw->setPrevAlive(true);
	
	// Penalise movement to encourage exploration
	return -0.2;
}

/*
 Makes sweeper move in optimal direction.
 Updates Q table.
*/
bool CQLearningController::Update(void) {

	using namespace std;

	uint cDead = std::count_if(m_vecSweepers.begin(), m_vecSweepers.end(),
						       [](CDiscMinesweeper * s)->bool{
								return s->isDead();
							   });

	if (cDead == CParams::iNumSweepers){
		printf("All dead ... skipping to next iteration\n");
		m_iTicks = CParams::iNumTicks;
	}

	for (auto sw : m_vecSweepers){
		if (sw->isDead()) continue;
		int stateIndex = 0;
		int actionIndex = 0;

		// Observe the current state
		stateIndex = (xDim) * sw->getYCell() + sw->getXCell();

		// Select action with highest historic return
		actionIndex = 0;
		double maxQ = Q[stateIndex][0];
		for (int a = 1; a < numActions; ++a) {
			double temp = Q[stateIndex][a];
			if (temp > maxQ) {
				maxQ = temp;
				actionIndex = a;
			}
		}

		/*
		//with prob 0.1, do a random action
		float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		if (r < 0)
			actionIndex = RandInt(0, 3);
		*/

		// Add the current state info to previous state to be used later
		State s;
		s.actionIndex = actionIndex;
		s.stateIndex = stateIndex;
		s.alive = true;
		sw->setPrevState(s);

		// Move in chosen direction
		if (actionIndex == 0)
			sw->setRotation(EAST);
		else if (actionIndex == 1)
			sw->setRotation(NORTH);
		else if (actionIndex == 2)
			sw->setRotation(WEST);
		else if (actionIndex == 3)
			sw->setRotation(SOUTH);

	}

	// Update the sweepers (move)
	CDiscController::Update(); 

	int i = 0;
	for (int i = 0; i < m_vecSweepers.size(); ++i){
		auto sw = m_vecSweepers[i];

		// Get previous state info
		State s = sw->getPrevState();

		if (s.alive == false) continue;

		// Calculate the new state index
		int newStateIndex = (xDim) * sw->getYCell() + sw->getXCell();

		// Find the maximum Q value for the given state (i.e. for the next action)
		double maxQ = Q[newStateIndex][0];
		for (int a = 1; a < numActions; ++a) {
			if (Q[newStateIndex][a] > maxQ) 
				maxQ = Q[newStateIndex][a];
		}

		// Update Q table
		// Q(s,a) <- Q(s,a) +  LR[R + y * max Q(s', a') - Q(s,a)] 
		Q[s.stateIndex][s.actionIndex] = Q[s.stateIndex][s.actionIndex] + 0.1 * (R(sw) + (0.9 * maxQ) - Q[s.stateIndex][s.actionIndex]);
	}

	return true;
}



CQLearningController::~CQLearningController(void)
{
	// RAII, nothing to cleanup
}
