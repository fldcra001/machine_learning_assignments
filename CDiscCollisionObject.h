#ifndef CDiscCOLLISIONOBJECT_H
#define CDiscCOLLISIONOBJECT_H
#include "SVector2D.h"
#include "CCollisionObject.h"
#include "CParams.h"
class CDiscCollisionObject: public CCollisionObject
{
public:
	CDiscCollisionObject();
	CDiscCollisionObject(ObjectType objectType, SVector2D<int> position);
	virtual ~CDiscCollisionObject();

	void setPosition(SVector2D<int> position);
	SVector2D<int> getPosition() const;

	int getXCell() {
		return m_vPosition->x / CParams::iGridCellDim;
	}

	int getYCell() {
		return m_vPosition->y / CParams::iGridCellDim;
	}
	
private:
	SVector2D<int> * m_vPosition;
};

#endif