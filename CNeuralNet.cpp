/*
                                                                           
   (               )                                        )              
 ( )\     )     ( /(       (                  (  (     ) ( /((             
 )((_) ( /(  (  )\())`  )  )(   (  `  )   (   )\))( ( /( )\())\  (   (     
((_)_  )(_)) )\((_)\ /(/( (()\  )\ /(/(   )\ ((_))\ )(_)|_))((_) )\  )\ )  
 | _ )((_)_ ((_) |(_|(_)_\ ((_)((_|(_)_\ ((_) (()(_|(_)_| |_ (_)((_)_(_/(  
 | _ \/ _` / _|| / /| '_ \) '_/ _ \ '_ \/ _` |/ _` |/ _` |  _|| / _ \ ' \)) 
 |___/\__,_\__||_\_\| .__/|_| \___/ .__/\__,_|\__, |\__,_|\__||_\___/_||_|  
                    |_|           |_|         |___/                         

 Creates an ANN by using backpropagation of errors

 Modified by Craig Feldman
 22 August 2014

 */

#include "CNeuralNet.h"
#include <iostream>
#include <ctime>

/**
 CONSTRUCTOR

 Allocates memory for input->hidden and hidden->output weights
*/
CNeuralNet::CNeuralNet(uint inputSize, uint hiddenSize, uint outputLayerSize, double lRate, double mse_cutoff)
							//+1 for a bias term
	: inputLayerSize(inputSize + 1), hiddenLayerSize(hiddenSize), outputLayerSize(outputLayerSize), lRate(lRate), mse_cutoff(mse_cutoff)
{
	std::cout << "Input Layer Size: " << inputSize << " | Hidden Layer Size: " << hiddenLayerSize << " | Output Layer Size: " << outputLayerSize << "\n\n";
	
	//allocate memory for weights
	for (int i = 0; i < inputLayerSize; ++i)
		inputToHiddenWeights.push_back(std::vector<double>());
	for (int i = 0; i < hiddenLayerSize; ++i)
		hiddenToOutputWeights.push_back(std::vector<double>());
	
	input.resize(inputLayerSize);
	hidden.resize(hiddenLayerSize);
	output.resize(outputLayerSize);

	initWeights();
}

/* Default constructur used by GA. */
CNeuralNet::CNeuralNet() {
	inputLayerSize = 4 + 1;
	outputLayerSize = 2;
	hiddenLayerSize = 5;
	//std::cout << "CONSTRUCTING ANN " << std::endl;

	// allocate memory for weights
	for (int i = 0; i < inputLayerSize; ++i)
			inputToHiddenWeights.push_back(std::vector<double>());
	for (int i = 0; i < hiddenLayerSize; ++i)
		hiddenToOutputWeights.push_back(std::vector<double>());

	input.resize(inputLayerSize);
	hidden.resize(hiddenLayerSize);
	output.resize(outputLayerSize);

	initWeights();
}

CNeuralNet::~CNeuralNet() {
	//used vectors, therefore RAII
}

/* Method to initialize both layers of weights to random numbers */
void CNeuralNet::initWeights(){
	// seed random # generator
	//srand((unsigned)(time(NULL)));

	//input to hidden layer weights
	for (int i = 0; i < inputLayerSize; ++i)
		for (int j = 0; j < hiddenLayerSize; ++j)
			inputToHiddenWeights[i].push_back(RandomClamped());

	//hidden layer to output layer weights
	for (int i = 0; i < hiddenLayerSize; ++i) {
		for (int j = 0; j < outputLayerSize; ++j)
			hiddenToOutputWeights[i].push_back(RandomClamped());
	}
}

/* Feeds the given input through the ANN and computes the output of hidden layer and output nodes. */
void CNeuralNet::feedForward(const std::vector<double> inputs) {
	//std::cout << "FEED FORWARD" << std::endl;
	//std::cout << inputToHiddenWeights[1][1] <<  std::endl;

	double sum;	

	// copy the paramter values to the input vector
	input = std::move(inputs);
	input.push_back(1); //add bias term
	
	// Assign activation value to each neuron using sigmoid function	
	// for each neuron in hidden layer
	for (int h = 0; h < hiddenLayerSize; ++h) {
		sum = 0;
		// for input from each neuron in input layer
		for (int i = 0; i < inputLayerSize - 1; ++i){
			// apply weights to inputs and add to sum
			// sum += weight i to h * value of input i
			sum += inputToHiddenWeights[i][h] * input[i];
		}
		// -1 due to the last element being a bias term
		sum += inputToHiddenWeights[inputLayerSize - 1][h];

		hidden[h] = sigmoid(sum);
	}

	// compute output
	// for each neuron in the output layer
	for (int o = 0; o < outputLayerSize; ++o) {
		sum = 0;
		// for input from each neuron in hidden layer
		for (int h = 0; h < hiddenLayerSize; ++h) {
			sum += hiddenToOutputWeights[h][o] * hidden[h];
		}

		output[o] = sigmoid(sum);
	}
}


/* Propagates the errors through the ANN and adjusts network weights accordingly */
void CNeuralNet::propagateErrorBackward(const std::vector<double> desiredOutput){
	using namespace std;

	// errors
	vector<double> hiddenErrors;
	vector<double> outputErrors;

	// calculate error at output layer
	for (int i = 0; i < outputLayerSize; ++i) {
		double error = sigmoidDerivative(output[i]) * (desiredOutput[i] - output[i]);
		outputErrors.push_back(error);
	}

	// calculate errors at hidden layer
	//for each neuron in the hidden layer
	for (int h = 0; h < hiddenLayerSize; ++h) {
		double sum = 0;
		//for each neuron in output layer
		for (int o = 0; o < outputLayerSize; ++o) {
			sum += hiddenToOutputWeights[h][o] * (desiredOutput[o] - output[o]);
		}
		double error = sigmoidDerivative(hidden[h]) * sum;
		hiddenErrors.push_back(error);
	}

	// Adjust the weights from the hidden to the output layer : learning rate * error at the output layer * error at the hidden layer
	
	//	for each connection between the hidden and output layers
	for (int h = 0; h < hiddenLayerSize; ++h) {
		for (int o = 0; o < outputLayerSize; ++o) {
			hiddenToOutputWeights[h][o] += lRate * outputErrors[o] * hiddenErrors[h];
		}
	}

	// Adjust the weights from the input to the hidden layer : learning rate * error at the hidden layer * input layer node value
	
	//for each connection between the input and hidden layers
	for (int i = 0; i < inputLayerSize ; ++i) {
		for (int h = 0; h < hiddenLayerSize; ++h) {
			inputToHiddenWeights[i][h] += lRate * hiddenErrors[h] * input[i];
		}
	}
}

/** Computes the MSE of the ANN */
long double CNeuralNet::meanSquaredError(const std::vector<double> desiredOutput){
	double sum = 0;
	for (int i = 0; i < outputLayerSize; ++i){
		long double error = desiredOutput[i] - output[i];
		sum = sum + (error * error);
	}

	return sum / outputLayerSize;
}
/**
This trains the neural network according to the back propagation algorithm.
The primary steps are:
for each training pattern:
  feed forward
  propagate backward
until the MSE becomes suitably small

Returns the MSE result from training
*/
double CNeuralNet::train(const std::vector<std::vector<double> > inputs,
	const std::vector<std::vector<double> > outputs, uint trainingSetSize) {
	
	using namespace std;

	int sizeOfTrainingData = inputs.size();
	long double mse = 0;

	cout << "Training..." << endl;

	// for each training pattern, feed the data forward and propagate the errors back
	for (int i = 0; i < sizeOfTrainingData - 1; ++i) {
		feedForward(inputs[i]);
		propagateErrorBackward(outputs[i]);
		mse += meanSquaredError(outputs[i]);
	}

	//only compute MSE at the end, for efficiency
	mse /= sizeOfTrainingData;
	
	cout << "Current MSE: " << mse << "\n" << endl;
	return mse;
}

/**
Classifies a given input as turn towards or turn away.

returns 0 for turn towards, 1 for turn away
*/
uint CNeuralNet::classify(const std::vector<double> input){
	feedForward(input);
	// if output[0] == 1, the turn towards, if output[1] == 1, then turn away (use > for rounding).
	return getOutput(0) > getOutput(1) ? 0 : 1;
}

/* returns the output at the specified index */
double CNeuralNet::getOutput(uint index) const{
	return output[index];
}

/* Returns sigmoid function value */
double CNeuralNet::sigmoid(double in) {
	return (1 / (1 + exp(-in)));
}

/* Returns sigmoid derivative */
double CNeuralNet::sigmoidDerivative(double in) {
	return (CNeuralNet::sigmoid(in)) * (1 - CNeuralNet::sigmoid(in));
}

/* Returns a vector containing all the weights */
std::vector<double> CNeuralNet::getWeights() const {
	
	std::vector<double> weights;
	weights.reserve(inputLayerSize * hiddenLayerSize + hiddenLayerSize * outputLayerSize);

	// For each node in input layer, add that nodes weights
	for (int i = 0; i < inputToHiddenWeights.size(); ++i)
		weights.insert(weights.end(), inputToHiddenWeights[i].begin(), inputToHiddenWeights[i].end());

	for (int i = 0; i < hiddenToOutputWeights.size(); ++i)
		weights.insert(weights.end(), hiddenToOutputWeights[i].begin(), hiddenToOutputWeights[i].end());

	return weights;
}

/* Returns number of weights in ANN */
int CNeuralNet::getNumWeights() const {
	return inputLayerSize * hiddenLayerSize + hiddenLayerSize * outputLayerSize;
}

/* Replaces all weights with given values */
void CNeuralNet::setWeights(std::vector<double> &weights) {
	int index = 0;
	// Add all input to hidden weights
	for (int i = 0; i < inputLayerSize; ++i)
		for (int h = 0; h < hiddenLayerSize; ++h)
			inputToHiddenWeights[i][h] = weights[index++];

	// Add all hidden to output weights
	for (int h = 0; h < hiddenLayerSize; ++h)
		for (int o = 0; o < outputLayerSize; ++o)
			hiddenToOutputWeights[h][o] = weights[index++];
	
}