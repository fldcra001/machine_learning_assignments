/**
    )                                                         
 ( /(                                (         )              
 )\())  (   (  (         (    )      )\  (  ( /((             
((_)\  ))\ ))\ )(   (    )\  /((  ( ((_)))\ )\())\  (   (     
 _((_)/((_)((_|()\  )\  ((_)(_))\ )\ _ /((_|_))((_) )\  )\ )  
| \| (_))(_))( ((_)((_) | __|)((_|(_) (_))(| |_ (_)((_)_(_/(  
| .` / -_) || | '_/ _ \ | _|\ V / _ \ | || |  _|| / _ \ ' \)) 
|_|\_\___|\_,_|_| \___/ |___|\_/\___/_|\_,_|\__||_\___/_||_|  

Further details can be found in:
X. Yao, Evolving artificial neural networks, Proceedings of the IEEE,
87(9):1423-1447, September 1999                                                              

Perfroms evolutionary functions on ANN

Craig Feldman
October 2014
*/

#include "CBasicEA.h"


double CBasicEA::crossOverRate;
double CBasicEA::mutationRate;
double CBasicEA::maxMutation;
double CBasicEA::mutationReduction;

/* Constructs a population of randomly initialised genomes */
CBasicEA::CBasicEA(int popSize, int numWeights, double crossOver, double mutation, double maxMut) :
	populationSize(popSize), numWeights(numWeights), generationNum(0), numElite(CParams::iNumElite) {

	crossOverRate = crossOver;
	mutationRate = mutation;
	maxMutation = maxMut;
	//how much to reduce mutation rate by with time
	mutationReduction = 0.05 * mutation;
	cout << "Algorithm initialised.\n| Crossover Rate = " << crossOverRate << " | Mutation Rate = " << mutation << " | Maximum Mutation = " << maxMutation << " |" << endl;

	//initialise population with random weights in ANN
	for (int i = 0; i < populationSize; ++i) {
		population.push_back(Genome());
		for (int j = 0; j < numWeights; ++j)
			population[i].weights.push_back(RandomClamped());
	}
}

/*
Runs the GA for one generation.
Uses elitist selection where fittest indivuals are selected to proceed to next generation without any mutation.
Remainder of new population is filled using crossover and mutation whereby parents are selected via roulette wheel sampling.
*/
vector<Genome> CBasicEA::run(vector<Genome>& oldPopulation){
	++generationNum;
	//cout << "\nGENERATION " << generationNum << endl;
	population = oldPopulation;

	//sort according to fitness
	sort(population.begin(), population.end());
	// Set best, worst and avg fitness
	setFitnessScores();

	//cout << totalFitness <<endl;

	// Store new population
	vector<Genome> newPopulation;
	
	// Reduce mutation rate with time (also increase number of elite to reach terminataion)
	if (generationNum % 5 == 0) {
		numElite += 1;
		mutationRate -= mutationReduction;
	}

	if (numElite > populationSize)
		numElite = populationSize;
	//CParams::iNumElite = numElite;	
 
	addBest(numElite, 1, newPopulation);
	while (newPopulation.size() < populationSize) {
		//Get two parents
		Genome p1 = getGenomeRoulette();
		Genome p2 = getGenomeRoulette();

		// children;
		vector<double> c1, c2;
		//Crossover parents to produce two offspring
		crossover(p1.weights, p2.weights, c1, c2);

		// Mutation
		mutate(c1);
		mutate(c2);

		// Add children to new population
		newPopulation.push_back(Genome(c1, 0));
		newPopulation.push_back(Genome(c2, 0));
	}

	population = newPopulation;

	// If added an extra child, remove it
	if (population.size() > populationSize)
		population.pop_back();

	return population;
}

/* Crosses over two parents' weights and produces two offspring with probabilty = crossOverRate */
void CBasicEA::crossover(const vector<double>& parent1, const vector<double>& parent2, vector<double>& child1, vector<double>& child2)
{
	if (RandFloat() > crossOverRate || (parent1 == parent2)) {
		child1 = parent1;
		child2 = parent2;
		return;
	}
	//get a crossover point
	int cp = RandInt(0, parent1.size() - 1);
	// crossover to get children
	for (int i = 0; i < cp; ++i) {
		child1.push_back(parent1[i]);
		child2.push_back(parent2[i]);
	}

	for (int i = cp; i < parent1.size(); ++i) {
		child1.push_back(parent2[i]);
		child2.push_back(parent1[i]);
	}

}

/* Mutates a vector of weights with probability of mutation on each element equal to mutationRate */
void CBasicEA::mutate(vector<double>& weights)
{
	for (int i = 0; i < weights.size(); ++i)
		if (RandFloat() < mutationRate) {
			// add or subtract small value to weight
			weights[i] += (RandomClamped() * maxMutation);
		}
}

/* Selects 'quantity' fittest individuals to proceed without mutation (elitism). */
void CBasicEA::addBest(int quantity, int copies, vector<Genome> & newPop) {
	while (quantity--) {
		for (int i = 0; i < copies; ++i)
			newPop.push_back(population[populationSize - 1 - quantity]);
	}
}

/*
Performs Roulette wheel sampling on the population of genomes.
Returns a genome with probability propotional to it's fitness.
i.e. A fitter genome is more likely to be selected, but there is still the chance of a less fit genome surving as it may aid future evolution.
*/
Genome CBasicEA::getGenomeRoulette() {
	double currentSum = 0;
	// Get a random # between zero and the total fitness
	double randomValue = RandFloat() * totalModifiedFitness;

	// Iterate backwards through population genomes i.e. from most fit down to least fit
	for (auto genome = population.rbegin(); genome != population.rend(); ++genome) {
		// Take care of negative values in fitness
		currentSum += genome->fitness + abs(worstFitness) + 1;
		if (currentSum >= randomValue)
			return *genome;
	}

}

/* 
Sets average, total and minimum fitness.
Also computes a modified total fitness used by roulette wheel sampling method.
*/
void CBasicEA::setFitnessScores() {
	totalFitness = 0;
	//used for roulette wheel sampling to account for negative fitness
	totalModifiedFitness = 0;

	bestFitness = population[0].fitness;
	worstFitness = population[0].fitness;

	for (int i = 1; i < populationSize; ++i) {
		if (population[i].fitness > bestFitness) {
			bestFitness = population[i].fitness;
			fittestGenome = i;
		}

		if (population[i].fitness < worstFitness) 
			worstFitness = population[i].fitness;

		totalFitness += population[i].fitness;
	}
	averageFitness = totalFitness / populationSize;

	for (int i = 0; i < populationSize; ++i) 
		totalModifiedFitness += population[i].fitness + abs(worstFitness) + 1;
}
